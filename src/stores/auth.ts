import type { User } from "@/entities";
import router from "@/router";
import { fetchLogin, fetchLogout, postRegister } from "@/service/auth-service";
import { defineStore } from "pinia";
import { ref } from "vue";


export const useAuth = defineStore('auth', () => {

    const user = ref<User>();
    const stored = localStorage.getItem('user');
    if(stored) {
        user.value = JSON.parse(stored);
    }

    async function login(email:string,password:string) {
        const data = await fetchLogin(email,password);
        localStorage.setItem('user', JSON.stringify(data));
        user.value = data;
    }
    async function logout() {
        await fetchLogout();
        localStorage.removeItem('user');
        user.value = undefined;
        router.push('/');
    }

    async function register(userData : User){
    const data = await postRegister(userData);
    localStorage.setItem('user', JSON.stringify(data))
    user.value = data; 

    }
    
    
    return {user,login,logout, register};
})