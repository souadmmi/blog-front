import type { Categorie } from "@/entities";
import axios from "axios";

export async function fetchAllCategorie() {
    const response = await axios.get<Categorie[]>('http://localhost:8080/api/categorie')
    return response.data
}

export async function fetchOneCategorie(id:any) {
    const response = await axios.get<Categorie>('http://localhost:8080/api/categorie/' +id)
    return response.data
}
