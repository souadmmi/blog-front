import type { Articles } from "@/entities";
import axios from "axios";

export async function fetchAllArticle() {
    const response = await axios.get<Articles[]>('http://localhost:8080/api/article')
    return response.data
}

export async function postOneArticle(article : Articles) {
    const response = await axios.post<Articles>('http://localhost:8080/api/article', article)
    return response.data
    
}
export async function fetchThreeLatest(){
    const response = await axios.get<Articles[]>('http://localhost:8080/api/article/threelatest')
    return response.data
}

export async function fetchAllOfCategorie(id:any) {
    const response = await axios.get<Articles[]>('http://localhost:8080/api/article/categorie/'+id)
    return response.data
}
export async function fetchThreeLatestOfCategorie(id:any) {
    const response = await axios.get<Articles[]>('http://localhost:8080/api/article/categorie/'+id)
    return response.data
}

export async function fetchOneArticle(id:any) {
    const response = await axios.get<Articles>('http://localhost:8080/api/article/'+id)
    return response.data
    
}
export async function updateArticle(article:Articles) {
    const response = await axios.put<Articles>('http://localhost:8080/api/article/'+article.id, article);
    return response.data;
}
export async function deleteArticle(id:any) {
    await axios.delete<void>('http://localhost:8080/api/article/'+id);
}

export async function fetchArticleOfUser(){
    const response = await axios.get<Articles[]>('http://localhost:8080/api/article/user')
    return response.data;
}


