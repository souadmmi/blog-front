export interface Articles{

    id?:number,
    title: string,
    content:string,
    picture: string,
    comments: string,
    vue: number,
    user?:User,
    categorie: string,
    date :string 
    
}

export interface Categorie{
    id?: number,
    name:string
}
export interface Commentaire{
    id?:number,
    content : string,
    author:string,
    date:string,
    article : string
}

export interface User {
    id?:number,
    email:string,
    name?:string,
    password?:string,
    role:string
}